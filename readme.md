Run your application

To generate the start scripts simply run:
$ mvn package

And then simply run the script. On Mac and Linux, the command is:
$ sh target/bin/webapp

On Windows the command is:
C:/> target/bin/webapp.bat